"use strict";

const toQ328 = n => Math.round(n * (1 << 28));
const fromQ328 = n => n / (1 << 28);
const toDeg = n => n * 180 / Math.PI;
const fromDeg = n => n * Math.PI / 180;
const toHex = (n, len) => "0x" + (n & (Math.pow(2, (len * 4)) - 1)).toString(16).padStart(len, "0");

let alphabody;
let varbody;
let canvas;
let ctx;
let line = 0;

let animationTime = 0.5;
let stepTime = 0.15;

const Value = ((rad) => {
    let o = {
        rad: rad,
        get val() {
            return this.rad;
        },
        set val(n) {
            return this.rad = n;
        },
        get q328() {
            return toQ328(this.rad);
        },
        set q328(n) {
            return this.rad = fromQ328(n);
        },
        get deg() {
            return toDeg(this.rad);
        },
        set deg(n) {
            return this.rad = fromDeg(n);
        },
    };
    return o;
});

const printers = {
    integer: v => v.val.toFixed(0),
    floating: v => v.val.toFixed(8),
    deg: v => v.deg.toFixed(8),
    q328: v => toHex(v.q328, 8),
    setQ328: () => {
        alpha_printer = printers.q328;
        variables.x.printer = printers.q328;
        variables.y.printer = printers.q328;
        variables.xs.printer = printers.q328;
        variables.ys.printer = printers.q328;
        variables.theta.printer = printers.q328;
        variables.phi.printer = printers.q328;
        updateView();
    },
    setRad: () => {
        alpha_printer = printers.floating;
        variables.x.printer = printers.floating;
        variables.y.printer = printers.floating;
        variables.xs.printer = printers.floating;
        variables.ys.printer = printers.floating;
        variables.theta.printer = printers.floating;
        variables.phi.printer = printers.floating;
        updateView();
    },
    setDeg: () => {
        alpha_printer = printers.deg;
        variables.x.printer = printers.floating;
        variables.y.printer = printers.floating;
        variables.xs.printer = printers.floating;
        variables.ys.printer = printers.floating;
        variables.theta.printer = printers.deg;
        variables.phi.printer = printers.deg;
        updateView();
    }
};

const alpha = (() => {
    let alpha = [];
    for (let i = 0; i < 29; i++) {
        alpha.push(Math.atan(1 / (1 << i)));
    }
    return alpha;
})();
const K = alpha.map(a => Math.cos(a)).reduce((a, b) => a * b, 1);

let variables = {
    i: { value: Value(0), color: 'green', printer: printers.integer },
    x: { value: Value(K), color: 'black', printer: printers.floating },
    y: { value: Value(0), color: 'black', printer: printers.floating },
    xs: { value: Value(0), color: 'black', printer: printers.floating },
    ys: { value: Value(0), color: 'black', printer: printers.floating },
    theta: { value: Value(0), color: 'blue', printer: printers.deg },
    phi: { value: Value(fromDeg(30)), color: 'purple', printer: printers.deg },
};
let alpha_printer = printers.deg;

let animations = {
    theta: { animated: false, begin: 0, current: 0, end: 0 },
    alpha_i: { animated: false, begin: 0, current: 0, end: 0 },
};

const animated = () => {
    return (
        animations.theta.animated ||
        animations.alpha_i.animated
    );
};

const renderAlpha = () => {
    alphabody.innerHTML = "";
    for (let i = 0; i < 29; i++) {
        let c = i === variables.i.value.val ? variables.i.color : 'black';

        let tr = document.createElement('tr');
        let td1 = document.createElement('td');
        td1.innerText = i;
        td1.style = "color: " + c;
        let td2 = document.createElement('td');
        td2.innerText = alpha_printer(Value(alpha[i]));
        td2.style = "color: " + c;
        tr.appendChild(td1);
        tr.appendChild(td2);
        alphabody.appendChild(tr);
    }
};

const renderVariables = () => {
    let keys = Object.keys(variables);
    keys.sort();
    varbody.innerHTML = "";
    for (let key of keys) {
        let c = variables[key].color;
        let tr = document.createElement('tr');
        let td1 = document.createElement('td');
        td1.innerText = key;
        td1.style = "color: " + c;
        let td2 = document.createElement('td');
        td2.innerText = variables[key].printer(variables[key].value);
        td2.style = "color: " + c;
        tr.appendChild(td1);
        tr.appendChild(td2);
        varbody.appendChild(tr);
    }
};

const renderCode = () => {
    let spans = document.querySelectorAll('.code span.line');
    spans.forEach((s, i) => {
        if (i === line) {
            s.classList.add('active');
        } else {
            s.classList.remove('active');
        }
    });
};

const drawAngle = (angle, color) => {
    let c = ctx.strokeStyle;
    ctx.strokeStyle = color;

    let x = Math.cos(angle) * 460 + 20;
    let y = 500 - Math.sin(angle) * 460;

    ctx.beginPath();
    ctx.moveTo(20, 500);
    ctx.lineTo(x, y);
    ctx.stroke();

    ctx.strokeStyle = c;
};

const drawXY = () => {
    let x = variables.x.value.val * 460 + 20;
    let y = 500 - variables.y.value.val * 460;

    ctx.beginPath();
    ctx.moveTo(x, 500);
    ctx.lineTo(x, y);
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(20, y);
    ctx.lineTo(x, y);
    ctx.stroke();
};

const drawBaseCoordinateSystem = () => {
    ctx.clearRect(0, 0, 500, 1000);

    ctx.beginPath();
    ctx.moveTo(0, 500);
    ctx.lineTo(500, 500);
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(20, 0);
    ctx.lineTo(20, 1000);
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(20, 500, 460, 0, 2 * Math.PI, true);
    ctx.stroke();
};

const updateView = () => {
    drawBaseCoordinateSystem();

    let theta_step = (animations.theta.end - animations.theta.begin) / (animationTime * 60);
    let alpha_i_step = (animations.alpha_i.end - animations.alpha_i.begin) / (animationTime * 60);
    let theta = animations.theta.animated ? animations.theta.current += theta_step : variables.theta.value.rad;
    let alpha_i = animations.alpha_i.animated ? animations.alpha_i.current += alpha_i_step : alpha[variables.i.value.val];
    if (
        (animations.theta.end < animations.theta.begin && theta <= animations.theta.end) ||
        (animations.theta.end > animations.theta.begin && theta >= animations.theta.end) ||
        theta_step === 0
    ) {
        theta = variables.theta.value.rad;
        animations.theta.animated = false;
    }
    if (
        (animations.alpha_i.end < animations.alpha_i.begin && alpha_i <= animations.alpha_i.end) ||
        (animations.alpha_i.end > animations.alpha_i.begin && alpha_i >= animations.alpha_i.end) ||
        alpha_i_step === 0
    ) {
        alpha_i = alpha[variables.i.value.val];
        animations.alpha_i.animated = false;
    }

    drawAngle(theta + alpha_i, variables.i.color);
    drawAngle(theta - alpha_i, variables.i.color);
    drawAngle(variables.phi.value.rad, variables.phi.color);
    drawAngle(theta, variables.theta.color);
    drawXY();
    renderAlpha();
    renderVariables();
    renderCode();

    if (animated()) window.requestAnimationFrame(updateView);
};

const performStep = () => {
    switch (line) {
        case 0: // x = 1
            variables.x.value.val = K;
            break;
        case 1: // y = 0
            variables.y.value.val = 0;
            break;
        case 2: // theta = phi
            variables.theta.value.rad = variables.phi.value.rad;
            break;
        case 3: // i = 0
            variables.i.value.val = 0;
            break;

        case 4: // while i < 29 do
            if (variables.i.value.val < 29) {
                line = 5;
                return;
            } else {
                line = 17;
                return;
            }

        case 5: // xs = y * 2^-i
            variables.xs.value.val = variables.y.value.val / (1 << variables.i.value.val);
            break;
        case 6: // ys = x * 2^-i
            variables.ys.value.val = variables.x.value.val / (1 << variables.i.value.val);
            break;

        case 7: // if theta <= 0 then
            if (variables.theta.value.rad <= 0) {
                line = 8;
                return;
            } else {
                line = 11;
                return;
            }

        case 8: // x = x + xs
            variables.x.value.val = variables.x.value.val + variables.xs.value.val;
            break;
        case 9: // y = y - xs
            variables.y.value.val = variables.y.value.val - variables.ys.value.val;
            break;
        case 10: // theta = theta + alpha[i]
            variables.theta.value.rad = variables.theta.value.rad + alpha[variables.i.value.val];
            line = 15;
            return;

        case 11: // else
            break;
        case 12: // x = x - xs
            variables.x.value.val = variables.x.value.val - variables.xs.value.val;
            break;
        case 13: // y = y + xs
            variables.y.value.val = variables.y.value.val + variables.ys.value.val;
            break;
        case 14: // theta = theta - alpha[i]
            variables.theta.value.rad = variables.theta.value.rad - alpha[variables.i.value.val];
            line = 15;
            return;

        case 15: // end if
            break;

        case 16: // i = i + 1
            variables.i.value.val = variables.i.value.val + 1;
            line = 4;
            return;

        case 17: // end while
            break;

        case 18:
            return;

        default:
            console.error("unknown line", line);
            line = 18;
            return;
    }

    line++;
};

const animStep = () => {
    animations.theta.begin   = animations.theta.current   = variables.theta.value.rad;
    animations.alpha_i.begin = animations.alpha_i.current = alpha[variables.i.value.val] || 0;
    performStep();
    if (animations.theta.begin !== (animations.theta.end = variables.theta.value.rad)) animations.theta.animated = true;
    if (animations.alpha_i.begin !== (animations.alpha_i.end = (alpha[variables.i.value.val] || 0))) animations.alpha_i.animated = true;
    updateView();
};

const simulation = {
    mode: 'step',
    reset: () => {
        let phi = parseFloat(document.getElementById('phibox').value);
        if (isNaN(phi)) {
            alert("invalid angle, defaulting to 30°");
            phi = 30;
        }

        simulation.mode = 'step';
        line = 0;
        variables.i.value.val     = 0;
        variables.x.value.val     = 0;
        variables.y.value.val     = 0;
        variables.xs.value.val    = 0;
        variables.theta.value.rad = 0;
        variables.phi.value.deg   = phi;
        animations.theta.animated = false;
        animations.alpha_i.animated = false;
        updateView();
    },
    step: () => {
        simulation.mode = 'step';
        animStep();
    },
    next: () => {
        const f = () => {
            if (simulation.mode !== 'next') return;
            animStep();
            if (line !== 4 && line !== 20) {
                if (animated()) {
                    setTimeout(f, animationTime * 1000);
                } else {
                    setTimeout(f, stepTime * 1000);
                }
            }
        }
        simulation.mode = 'next';
        f();
    },
    run: () => {
        const f = () => {
            if (simulation.mode !== 'run') return;
            animStep();
            if (line !== 20) {
                if (animated()) {
                    setTimeout(f, animationTime * 1000);
                } else {
                    setTimeout(f, stepTime * 1000);
                }
            }
        }
        simulation.mode = 'run';
        f();
    }
};

window.addEventListener('DOMContentLoaded', () => {
    alphabody = document.getElementById('alphabody');
    varbody = document.getElementById('varbody');
    canvas = document.getElementById('canvas');
    ctx = canvas.getContext('2d');

    document.getElementById('reset').addEventListener('click', () => simulation.reset());
    document.getElementById('step').addEventListener('click', () => simulation.step());
    document.getElementById('next').addEventListener('click', () => simulation.next());
    document.getElementById('run').addEventListener('click', () => simulation.run());

    updateView();
});

const banner = () => {
    console.log(`
    interactive        _ _
      ___ ___  _ __ __| (_) ___
     / __/ _ \\| '__/ _\` | |/ __|
    | (_| (_) | | | (_| | | (__
     \\___\\___/|_|  \\__,_|_|\\___|
                      calculator

    ~Ferdinand Bachmann, SS2019~

    Configuration Variables:
      - animationTime      float, number of seconds each animation should take
      - stepTime           float, number of seconds each non-animated step should take

    Functions
      - printers.setDeg()  set variable display to decimal, angles in degrees
      - printers.setRad()  set variable display to decimal, angles in radians
      - printers.setQ328() set variable display to hexadecimal Q1.14, angles in radians

      - simulation.reset() reset simulation and load phi value
      - simulation.step()  step simulation forward by one line
      - simulation.next()  step simulation forward by one iteration of the loop
      - simulation.run()   run the simulation until it is finished

      - banner():          display this message

    `);
};

banner();
